let CACHE_ACTUAL = 'cache0';
let archivos_para_cachear = [
  '/',
  '/?o=i',

  // '/manifest.json',
  // '/styles/main.css',

  '/index.js',
  '/app.js',

];

self.addEventListener('install', e => {
  console.log('install');
  e.waitUntil(
    caches.open(CACHE_ACTUAL).then(cache => {
      
      return cache.addAll(archivos_para_cachear)
        .then(_ => {
          self.skipWaiting();
        });
    })
  );
});


self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());

  event.waitUntil(
    caches.keys().then(lasCachesQueExisten => {

      return Promise.all(
        lasCachesQueExisten.map(unaCache => {
          console.log(unaCache);
          if (unaCache !== CACHE_ACTUAL) {
            return caches.delete(unaCache);
          }
        })
      );

    })
  );
});



self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(CACHE_ACTUAL).then(cache => {
      if(event.request.method === 'GET') {

        return cache.match(event.request).then(elementoCacheado => {
          const elementoDeLaRed = fetch(event.request).then(respuestaDeLaRed => {
            cache.put(event.request, respuestaDeLaRed.clone());
            return respuestaDeLaRed;
          })
          return elementoCacheado || elementoDeLaRed;
        })

      }
    })
  );
});