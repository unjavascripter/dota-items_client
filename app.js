let baseUrl;

fetch('http://localhost:3000/api/items')
  .then(respuesta => respuesta.json())
  .then(respuestaJsoneada => {
    
    const itemObj = respuestaJsoneada.data.itemdata;
    baseUrl = respuestaJsoneada.data.baseUrl;

    return Object.keys(itemObj).reduce((acc, item) => {
      acc = acc || [];
      acc.push(itemObj[item]);
      return acc;
    }, []);
  })
  .then(itemsArr => {
    const container = document.querySelector('.items-container');

    itemsArr.forEach(item => {
      console.log(item);
      let itemDiv = document.createElement('div');
      let itemImage = new Image();
      let contentDiv = document.createElement('div');

      itemDiv.classList.add('item');
      contentDiv.classList.add('item-content');

      itemImage.onload = function(){
          itemDiv.appendChild(itemImage);   
      };
      itemImage.width = 85;
      itemImage.height = 64;
      itemImage.src = baseUrl + item.img;

      contentDiv.innerHTML += `
        <div class="item-name">
          <b>${item.dname}</b>
        </div>
        <div class="item-cost">
          <b>${item.cost}</b>
        </div>
        ${item.desc ? `<div class="item-description">${item.desc}</div>` : ''}
        <div class="item-attributes">${item.attrib}</div>
        <div class="item-notes">${item.notes}</div>
        <div class="item-lore"><i>${item.lore}</i></div>
      `;
      
      itemDiv.appendChild(contentDiv)
      container.appendChild(itemDiv);

    })
  })
