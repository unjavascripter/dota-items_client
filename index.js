'use strict';

if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./sw.js', {scope: './'})
  .then(registration => {
    registration.onupdatefound = _ => {
      const installingWorker = registration.installing;
      console.log(installingWorker);
      installingWorker.onstatechange = _ => {
        console.log(installingWorker.state);
        if(installingWorker.state === 'installed') {
          if(!navigator.serviceWorker.controller) {
            console.log('Sitio \'cacheado\' ¡Visitas offline activadas!');
          }
        }

      };
    };
  }).catch(error => {
    console.log(error);
  });
}

